<?php

/*
    Plugin Name: WP Category Media
    Plugin URI: ...
    Description: A plugin that adds media fields to category.
    Version: 0.1
    Author: EmgrtE
    Author URI: https://github.com/EmgrtE
*/

// global "wpcm" vars
$wpcm_name = plugin_basename(__FILE__);
$wpcm_path = plugin_dir_path(__FILE__);
$wpcm_url = plugin_dir_url(__FILE__);

// include functions for add "wpcm" languages support
include_once($wpcm_path . 'includes/includes-add-languages.php');

// include functions for add "wpcm" assets
include_once($wpcm_path . 'includes/includes-add-assets.php');

// include functions for add "wpcm" meta fields
include_once($wpcm_path . 'includes/includes-add-meta.php');

// include functions to change category content output
include_once($wpcm_path . 'includes/includes-change-content.php');

/*
    ___________________________
    | q w e r t y u i o p [ ] |
    |  a s d f g h j k l ; '  |
    |   z x c v b n m , . /   |
    |=========================|
*/