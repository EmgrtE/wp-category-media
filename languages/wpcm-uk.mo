��          \      �       �   	   �      �   J   �      <     N     [  ?   h  �  �  !   �  @   �  �   �     n  %   �     �  u   �                                       Add image Choose an image for category. Insert a link of video for category. Only YouTube and Vimeo are supported. Media of category Remove image Remove video Your link is not from YouTube or Vimeo. Please check your link. Project-Id-Version: WP Category Media
POT-Creation-Date: 2019-05-21 02:06+0300
PO-Revision-Date: 2019-05-21 02:08+0300
Last-Translator: 
Language-Team: EmgrtE
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.3
X-Poedit-Basepath: .
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
 Додати зображення Виберіть зображення для категорії. Вставте посилання на відео для категорії. Підтримуються лише YouTube і Vimeo. Медіа категорії Видалити зображення Видалити відео Ваше посилання не з YouTube або Vimeo. Будь ласка, перевірте посилання. 