��          \      �       �   	   �      �   J   �      <     N     [  ?   h  �  �  '   �  >   �  �   �     t  %   �     �  s   �                                       Add image Choose an image for category. Insert a link of video for category. Only YouTube and Vimeo are supported. Media of category Remove image Remove video Your link is not from YouTube or Vimeo. Please check your link. Project-Id-Version: WP Category Media
POT-Creation-Date: 2019-05-21 02:02+0300
PO-Revision-Date: 2019-05-21 02:05+0300
Last-Translator: 
Language-Team: EmgrtE
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.3
X-Poedit-Basepath: .
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
 Добавить изображение Выберите изображение для рубрики. Вставьте ссылку на видео для категории. Поддерживаются только YouTube и Vimeo. Медия рубрики Удалить изображение Удалить видео Ваша ссылка не с YouTube или Vimeo. Пожалуйста, проверьте вашу ссылку. 