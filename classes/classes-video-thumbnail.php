<?php

// wpcm_classes_video_thumbnail
class wpcm_classes_video_thumbnail {
    /*
        get domain from link
        examples of link:
        - https://www.youtube.com/...
        - https://youtu.be/...
        - https://vimeo.com/...
        - https://player.vimeo.com/...
    */
    private function get_link_host($video_link) {
        if (!isset($video_link)) {
            return false;
        }

        $video_link_arr = parse_url($video_link);

        preg_match("/^(?:www\.)?(.*?)\.(?:com|co\.uk|be)$/", $video_link_arr['host'], $video_link_matches);

        if ($video_link_matches[1] == 'youtube' or $video_link_matches[1] == 'youtu') {
            return 'youtube';
        } elseif ($video_link_matches[1] == 'vimeo' or $video_link_matches[1] == 'player.vimeo') {
            return 'vimeo';
        } else {
            return false;
        }
    }

    // get id video from youtube link
    private function get_id_video_youtube($video_link) {
        if (!isset($video_link)) {
            return false;
        }

        // youtube regexp
        $youtube_parse_regexp = '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i';

        // parse
        if (preg_match($youtube_parse_regexp, $video_link, $video_link_match)) {
            return $video_link_match[1];
        } else {
            return false;
        }
    }

    /*
        get id video from vimeo link
        examples of link:
        - http://vimeo.com/[id]
        - http://player.vimeo.com/video/[id]
        - http://vimeo.com/channels/[channel_name]/[id]
    */
    private function get_id_video_vimeo($video_link) {
        if (!isset($video_link)) {
            return false;
        }

        // vimeo regexp
        $vimeo_parse_regexp = '/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/';

        // parse
        if (preg_match($vimeo_parse_regexp, $video_link, $video_link_match)) {
            return $video_link_match[count($video_link_match) - 1];
        } else {
            return false;
        }
    }

    // get id of video link
    public function get_video_id($video_link) {
        if (!isset($video_link)) {
            return false;
        }

        $service_name = $this->get_link_host($video_link);

        if ($service_name == 'youtube') {
            $youtube_video_id = $this->get_id_video_youtube($video_link);

            $youtube_result = array(
                'service' => $service_name,
                'id' => $youtube_video_id
            );

            return $youtube_result;
        } elseif ($service_name == 'vimeo') {
            $vimeo_video_id = $this->get_id_video_vimeo($video_link);

            $vimeo_result = array(
                'service' => $service_name,
                'id' => $vimeo_video_id
            );

            return $vimeo_result;
        } else {
            return false;
        }
    }

    // get link to thumbnail of video by id video
    public function get_thumbnail_link($video_link) {
        if (!isset($video_link)) {
            return false;
        }

        $service_name = $this->get_link_host($video_link);

        if ($service_name == 'youtube') {
            $youtube_video_id = $this->get_id_video_youtube($video_link);

            return 'https://img.youtube.com/vi/' . $youtube_video_id . '/0.jpg';
        } elseif ($service_name == 'vimeo') {
            $vimeo_video_id = $this->get_id_video_vimeo($video_link);

            $vimeo_video_json = file_get_contents('http://vimeo.com/api/v2/video/' . $vimeo_video_id . '.json');
            $vimeo_video_arr = json_decode($vimeo_video_json);

            return $vimeo_video_arr[0]->thumbnail_medium;
        } else {
            return false;
        }
    }
}