<?php

// add languages support
function wpcm_add_languages() {
    global $wpcm_name;

    /*
        language files created by "poedit" without "pot" file.
        file names consist of a domain name plus a locale name.
        examples of file names:
        - wpcm-en_US
        - wpcm-ru_RU
        - wpcm-uk
    */
    load_plugin_textdomain('wpcm', false, dirname($wpcm_name) . '/languages/' );
}

add_action('plugins_loaded', 'wpcm_add_languages');