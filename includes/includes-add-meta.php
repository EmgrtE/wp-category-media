
<?php

// add meta fields for category
function wpcm_add_meta($term) {
    global $wpcm_name;
    global $wpcm_path;

    /*
        get meta data for category
        more about geting meta fields for term: https://developer.wordpress.org/reference/functions/get_term_meta/
    */
    $wpcm_meta_value = get_term_meta($term->term_id, 'wpcm_data', true);

    // check if video link is available and get thumbnail of video
    if (isset($wpcm_meta_value['video']) and strlen($wpcm_meta_value['video']) > 1) {
        include_once($wpcm_path . 'classes/classes-video-thumbnail.php');

        $wpcm_video_thumb_object = new wpcm_classes_video_thumbnail;
        $wpcm_video_thumb_link = $wpcm_video_thumb_object->get_thumbnail_link($wpcm_meta_value['video']);
    }
    ?>

    <!-- wpcm form-field -->
    <tr class="form-field">
        <!-- wpcm form-field label -->
        <th scope="row" valign="top">
            <label for="wpcm-button-add"><?php _e('Media of category', 'wpcm'); ?></label>
        </th>
        <!-- end of wpcm form-field label -->

        <!-- wpcm form-field content -->
        <td>
            <!-- wpcm-container -->
            <div class="wpcm-container">
                <!-- wpcm-container-block -->
                <div class="wpcm-container-block wpcm-container-block__img">
                    <!-- wpcm-container-block-columns -->
                    <div class="wpcm-container-block-columns">
                        <!-- wpcm-container-block-columns-item -->
                        <div class="wpcm-container-block-columns-item wpcm-container-block-columns-item__img">
                            <div id="wpcm-image-container"
                                 class="wpcm-container-preview"
                                 style="display: <?php echo isset($wpcm_meta_value['img']) ? 'block' : 'none'; ?>;">
                                <?php if (isset($wpcm_meta_value['img'])) : ?>

                                <?php echo $wpcm_image_src = wp_get_attachment_image($wpcm_meta_value['img'], 'thumbnail'); ?>

                                <?php endif; ?>
                            </div>
                        </div>
                        <!-- end of wpcm-container-block-columns-item -->

                        <!-- wpcm-container-block-columns-item -->
                        <div class="wpcm-container-block-columns-item wpcm-container-block-columns-item__button">
                            <button id="wpcm-button-image-add"
                                    type="button"
                                    class="button"><?php _e('Add image', 'wpcm'); ?></button>
                        </div>
                        <!-- end of wpcm-container-block-columns-item -->

                        <!-- wpcm-container-block-columns-item -->
                        <div class="wpcm-container-block-columns-item wpcm-container-block-columns-item__button">
                            <button id="wpcm-button-image-remove"
                                    type="button"
                                    class="button"><?php _e('Remove image', 'wpcm'); ?></button>
                        </div>
                        <!-- end of wpcm-container-block-columns-item -->
                    </div>
                    <!-- end of wpcm-container-block-columns -->

                    <!-- wpcm-container-block-description -->
                    <p id="wpcm-image-description"
                       class="wpcm-container-block-description description"><?php _e('Choose an image for category.', 'wpcm'); ?></p>
                    <!-- end of wpcm-container-block-description -->
                </div>
                <!-- end of wpcm-container-block -->

                <!-- wpcm-container-block -->
                <div class="wpcm-container-block wpcm-container-block__video">
                    <!-- wpcm-container-block-columns -->
                    <div class="wpcm-container-block-columns">
                        <!-- wpcm-container-block-columns-item -->
                        <div class="wpcm-container-block-columns-item wpcm-container-block-columns-item__img">
                            <div id="wpcm-video-container"
                                 class="wpcm-container-preview"
                                 style="display: <?php echo $wpcm_video_thumb_link ? 'block' : 'none'; ?>;">
                                <?php if ($wpcm_video_thumb_link) : ?>

                                <img src="<?php echo $wpcm_video_thumb_link; ?>" alt="">

                                <?php endif; ?>
                            </div>
                        </div>
                        <!-- end of wpcm-container-block-columns-item -->

                        <!-- wpcm-container-block-columns-item -->
                        <div class="wpcm-container-block-columns-item wpcm-container-block-columns-item__button">
                            <input id="wpcm-video-input"
                                   type="text"
                                   name="wpcm_meta_value[video]"
                                   value="<?php echo isset($wpcm_meta_value['video']) ? $wpcm_meta_value['video'] : ''; ?>">
                        </div>
                        <!-- end of wpcm-container-block-columns-item -->

                        <!-- wpcm-container-block-columns-item -->
                        <div class="wpcm-container-block-columns-item wpcm-container-block-columns-item__button">
                            <button id="wpcm-button-video-remove"
                                    type="button"
                                    class="button"><?php _e('Remove video', 'wpcm'); ?></button>
                        </div>
                        <!-- end of wpcm-container-block-columns-item -->
                    </div>
                    <!-- end of wpcm-container-block-columns -->

                    <!-- wpcm-container-block-description -->
                    <?php if ($wpcm_video_thumb_link === false) : ?>

                    <p id="wpcm-video-error"
                       class="wpcm-container-block-error description"><?php _e('Your link is not from YouTube or Vimeo. Please check your link.', 'wpcm'); ?></p>

                    <?php endif; ?>

                    <p id="wpcm-video-description"
                       class="wpcm-container-block-description description"><?php _e('Insert a link of video for category. Only YouTube and Vimeo are supported.', 'wpcm'); ?></p>
                    <!-- end of wpcm-container-block-description -->
                </div>
                <!-- end of wpcm-container-block -->
            </div>
            <!-- end of wpcm-container -->

            <!-- wpcm hidden inputs -->
            <input id="wpcm-img-input"
                   type="hidden"
                   name="wpcm_meta_value[img]"
                   value="<?php echo isset($wpcm_meta_value['img']) ? $wpcm_meta_value['img'] : ''; ?>">

            <?php wp_nonce_field($wpcm_name, 'wpcm_meta_nonce'); ?>
            <!-- end of wpcm hidden inputs -->
        </td>
        <!-- end of wpcm form-field content -->
    </tr>
    <!-- end of wpcm form-field -->

    <?php
}

add_action('category_edit_form_fields', 'wpcm_add_meta');
add_action('category_add_form_fields', 'wpcm_add_meta');



// update meta fields for category
function wpcm_update_meta($term_id) {
    global $wpcm_name;

    if (!isset($_POST['wpcm_meta_value'])) {
        return;
    }

    /*
        verify request for update fields
        more about verify fields: https://codex.wordpress.org/Function_Reference/wp_nonce_field
    */
    if (!wp_verify_nonce($_POST['wpcm_meta_nonce'], $wpcm_name)) {
        return;
    }

    /*
        check user roles for the capability to edit terms
        more about roles: https://codex.wordpress.org/Roles_and_Capabilities
        'edit_term' for 4.7 or higher version of wordpress: https://make.wordpress.org/core/2016/10/28/fine-grained-capabilities-for-taxonomy-terms-in-4-7/
    */
    if (!current_user_can('edit_term', $term_id)) {
        return;
    }

    /*
        update meta fields
        more about update meta fields for term: https://codex.wordpress.org/Function_Reference/update_term_meta
    */
    $wpcm_meta_value = $_POST['wpcm_meta_value'];

    update_term_meta($term_id, 'wpcm_data', $wpcm_meta_value);
}

add_action('edited_category', 'wpcm_update_meta');
add_action('create_category', 'wpcm_update_meta');