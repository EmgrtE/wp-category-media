<?php

// add wp media scripts
function wpcm_add_media_api() {
    $screen = get_current_screen();

    // check if current page is "edit category" in the admin panel
    if ($screen->id == 'edit-category' && $screen->taxonomy == 'category') {
        wp_enqueue_media();
    }
}

add_action('admin_enqueue_scripts', 'wpcm_add_media_api');

// add "wpcm" assets
function wpcm_assets() {
    global $wpcm_url;

    $screen = get_current_screen();

    // check if current page is "edit category" in the admin panel
    if ($screen->id == 'edit-category' && $screen->taxonomy == 'category') {
        wp_enqueue_script('wpcm_script', $wpcm_url . '/assets/js/script.js');

        wp_enqueue_style('wpcm_style', $wpcm_url . '/assets/css/style.css');
    }
}

add_action('admin_head', 'wpcm_assets');