<?php

// add media before content of category
function wpcm_before_content($content) {
    global $wpcm_path;

    if (is_category()) {
        // render template to buffer
        ob_start();
        include_once($wpcm_path . 'templates/templates-category-content.php');
        $beforecontent .= ob_get_clean();

        $content = $beforecontent . $content;
    }
    
    return $content;
}

add_filter('category_description', 'wpcm_before_content');