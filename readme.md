# WP Category Media

A plugin that adds media fields (image and video) to category.

## How-to install

Copy `wp-category-media` directory to your wp plugin directory (for example: `[wp location]/wp-content/plugins/`) and activate **WP Category Media** in admin panel.

## License

GNU GPL v2.0