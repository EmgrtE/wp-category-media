'use strict';

// create actions to "wpcm" buttons
function wpcmButtons() {
    // add action to a button to add an image to category
    jQuery('#wpcm-button-image-add').on('click', function(e) {
        e.preventDefault();

        // get text for media popup window
        var descriptionText = jQuery('#wpcm-image-description').text(),
            buttonText = jQuery(this).text();

        /*
            set settings to wp.media to create media popup window
            more about wp media api: https://codex.wordpress.org/Javascript_Reference/wp.media
        */
        if (!mediaFrame) {
            var mediaFrame = wp.media({
                multiple: false,
                library: {
                    type: 'image'
                },
                button: {
                    text: buttonText
                },
                title: descriptionText
            });
        }

        // open media popup window
        mediaFrame.open();

        // get data from image when it's selected
        mediaFrame.on('select', function(data) {
            var imageData = mediaFrame.state().get('selection').first().toJSON();

            jQuery('#wpcm-img-input').val(imageData.id);

            // check if img element is available
            if (jQuery('#wpcm-image-container img').length) {
                jQuery('#wpcm-image-container img').attr('srcset', '');
                jQuery('#wpcm-image-container img').attr('src', imageData.url);
                jQuery('#wpcm-image-container').css('display', 'block');
            } else {
                var imageObject = new Image();

                imageObject.src = imageData.url;

                jQuery('#wpcm-image-container').append(imageObject);
                jQuery('#wpcm-image-container').css('display', 'block');
            }
        });
    });

    // add action to a button to remove an image from category
    jQuery('#wpcm-button-image-remove').on('click', function(e){
        e.preventDefault();

        jQuery('#wpcm-image-container img').remove();
        jQuery('#wpcm-image-container').css('display', 'none');
        jQuery('#wpcm-img-input').val('');
    });

    // add action to a button to remove an video from category
    jQuery('#wpcm-button-video-remove').on('click', function(e){
        e.preventDefault();

        jQuery('#wpcm-video-container img').remove();
        jQuery('#wpcm-video-container').css('display', 'none');
        jQuery('#wpcm-video-input').val('');
    });
}

// start functions after DOM ready
jQuery(document).ready(function() {
    wpcmButtons();
});