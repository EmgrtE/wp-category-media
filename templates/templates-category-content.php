<?php
global $wpcm_path;

// get category meta data of media
$category_object = get_queried_object();
$category_meta = get_term_meta($category_object->term_id, 'wpcm_data', true);
?>

<?php if ($category_meta) : ?>

<!-- wpcm-content -->
<div class="wpcm-content">
    <!-- wpcm-content-image -->
    <div class="wpcm-content-image">
        <?php if ($category_meta['img'] and strlen($category_meta['img']) > 1) : ?>

        <?php echo $image_src = wp_get_attachment_image($category_meta['img'], 'large'); ?>

        <?php endif; ?>
    </div>
    <!-- end of wpcm-content-image -->

    <!-- wpcm-content-video -->
    <div class="wpcm-content-video">
        <?php if ($category_meta['video'] and strlen($category_meta['video']) > 1) : ?>

        <?php
        include_once($wpcm_path . 'classes/classes-video-thumbnail.php');

        $video_object = new wpcm_classes_video_thumbnail;
        $video_link = $video_object->get_video_id($category_meta['video']);
        ?>

        <?php if ($video_link['service'] == 'youtube') : ?>

        <iframe src="https://www.youtube.com/embed/<?php echo $video_link['id']; ?>"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                width="640"
                height="360"
                frameborder="0"
                allowfullscreen></iframe>

        <?php elseif ($video_link['service'] == 'vimeo') : ?>

        <iframe src="https://player.vimeo.com/video/<?php echo $video_link['id']; ?>"
                allow="autoplay; fullscreen"
                width="640"
                height="360"
                frameborder="0"
                allowfullscreen></iframe>

        <?php endif; ?>

        <?php endif; ?>
    </div>
    <!-- end of wpcm-content-video -->
</div>
<!-- end of wpcm-content -->

<?php endif; ?>